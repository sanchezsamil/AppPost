# Aplicacion Movil AppPost 

Este proyecto esta generado con [Ionic CLI](https://github.com/angular/angular-cli) version 3.9.2.
 

# Proceso de instalacion 

Primero debes intalar Node.js (version v8.11.3), en tu computadora. A continuacion instala Ionic la ultima, con la herremienta de linea de comando en tu terminal. 

##  Con este comando instalas Ionic 
    npm install -g ionic

Luego debes descargar el proyecto del servidor de [Gitlab](https://gitlab.com/sanchezsamil/AppPost.git) ejecutando en la linea de comando git clone

##  Con este comando descargan el proyecto del servidor git lab de forma local 
    git clone https://gitlab.com/sanchezsamil/AppPost.git

Posteriormente estando en la linea de comandos (terminal), accede a la carpeta AppPost y ejecuta lo siguiente:

1. npm install (Instala las dependencias para que levante la aplicacion)
2. ionic serve (Levanta la aplicacion en un [Servidor Local](http://localhost:8100)  por defecto) 

Con esto ya tienes instalado el proyecto para poder continuar con el desarrollo. 

Muchas Gracias...

# dependedencias 
Node.js v8.11.3

# recursos necesarios
git version 1.9.1


#########################################################################################################

# Documentacion de Uso y Instalacion en Dipositivo Movil 

La Aplicacion Movil AppPost perimite administrar los post que han echo muchas personas cumpliendo con
las cuatro funcionalidades basicas: 

1. Buscar Post
2. Crear Post
3. Editar Post
4. Eliminar Post


La Aplicacion Movil AppPost cuenta con dos vista solamente: 

- La primera lista todos los post actulizados y cuenta con un buscador que solo te permite encontrar el post por el id del usuario que lo escribio.
- La segunda te permite crear un nuevo post.

Las opcion de edicion y eliminacion se encuntran rapida mente cuando se listan los post esto permite mayor rapidez y comodidad de admnistrarlos facilmente, simplemente buscas el post, lo ves, lo editas o lo eliminas de acuerdo a sus necesidades. 

# Buscar un Post

Para localizar un post solo necesitas saber el id del usuario que lo publico, colocarlo en el buscardo y listo te parecera las lista de post de ese usuario. 

# Crear un Post

Ve a la seccion Agregar, aqui se te desplegara un formulario en el cual debes introducir los siguiente campos:

1. Id Usuario:  Es el identificador de quien escribe el post. 
2. Titulo: Texto corto que resuma tu post
3. Mensaje: Descripcion mas detallada de lo que trata el post 

# Editar un Post

Es sencillo simplemente ubicas el post por el usuario que lo escribio y luego de encontrarlo al lado izquierdo tienes una herramienta con la opcion de editarlo. 

Al Darle click te aparecera un formulario con la informacion del post, simplemente cambiala por la nueva informacion y darle en el boton Guardar Post.

# Eliminar un Post

Es sencillo simplemente ubicas el post por el usuario que lo escribio y luego de encontrarlo al lado izquierdo tienes una herramienta con la opcion de eliminarlo. 

Antes de eliminarlo te mostrar una modal de confirmacion, mostrandote la informacion del post, aqui tienes dos opcion volver para atras o confirmar la eliminacion del post. 


# Instalacion en el dispositivo movil

Tienes que seguir estos paso:

1. Descargar en tu dipositivo movil la aplicacion [Ionic View](https://play.google.com/store/apps/details?id=com.ionicframework.view) 
2. Crearse una cuenta en Ionic Framework 
3. Luego de haber descargado el proyecto AppPost de git lab, debes subirlo al servidor de Ionic Framework (a tu cuenta). 
4. Luego desde el dispositivo movil entre en la apliacion Ionic View e instalar la aplicacion AppPost. 
5. Ejecutar en el telefono la Apliacion Apppost. 



