import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

// Importamos los servicios y modulos que necesitemos 
import { PostService } from '../../../app/services/post.services';

// IMPORTANDO MODULO PARA LA GENERAR MENSAJE DE ALERTA
import { AlertController } from 'ionic-angular';


@Component({
  selector: 'page-editar-post',
  templateUrl: 'editar-post.html',
})
export class EditarPostComponent {
  // VARIABLES
  idPost: number;
  post: object = {};
  postGuadado: any;

  constructor ( 
    public navParams: NavParams,
    private _post: PostService,
    private alertCtrl: AlertController,
    public viewCtrl: ViewController
  ) {
    this.idPost =  navParams.data['id'];
    
    // OBTENER UN POST
    this._post.obtenerPost( this.idPost , (post) => {
      this.post = {
        userId: post.userId,
        title: post.title,
        body: post.body
      }
    }, (err) => {});
    
  }

  // GUARDAR UN POST 
  guardarPost () {
     // SOBRESCRIBIR LO DATOS DE UN POST
     this._post.guardarPost( this.post, this.idPost, (post) => {
      this.postGuadado = post;
      this.mostrarAlertaExitosa();
    }, (err) => {
      console.log('Errror :: ' , err);
      this.mostrarAlertaFallida();
    });

  }

  // ALERTA PARA INDICAR GUARDADO EXITOSO DE UN POST
  mostrarAlertaExitosa() {
    const alert = this.alertCtrl.create({
      title: 'Exito',
      subTitle: 'Se edito el Post de manera exitosa!!!',
      buttons: ['Aceptar']
    });
    alert.present();
    this.viewCtrl.dismiss();
  }

  // ALERTA PARA INDICAR GUARDADO FALLIDO DE UN POST
  mostrarAlertaFallida() {
    const alert = this.alertCtrl.create({
      title: 'Fallida !!!',
      subTitle: 'Nos pudo editar el post !!! Intente Nuevamente',
      buttons: ['Aceptar']
    });
    alert.present();
  }

}

