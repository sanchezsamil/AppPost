import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

// Importamos los servicios y modulos que necesitemos 
import { PostService } from '../../app/services/post.services';

// IMPORTANDO MODULO PARA LA GENERAR MENSAJE DE ALERTA
import { AlertController } from 'ionic-angular';


@Component({
  selector: 'app-modal-eliminar-post',
  templateUrl: 'modal-eliminar-post.html',
})
export class ModalEliminarPostComponent {
  // VARIABLES
  idPost: number;
  post: object = {};
  postGuadado: any;

  constructor ( 
    public navParams: NavParams,
    private _post: PostService,
    private alertCtrl: AlertController,
    public viewCtrl: ViewController

  ) {
    // CAPTURANDO ID DEL POST
    this.idPost =  navParams.data['id'];
    
    // OBTENER UN POST
    this._post.obtenerPost( this.idPost , (post) => {
      this.post = {
        userId: post.userId,
        title: post.title,
        body: post.body
      }
    }, (err) => {});
    
  }

  // ELIMINAR UN POST
  eliminarPost () {
     // SOBRESCRIBIR LO DATOS DE UN POST
     this._post.eliminarPost(this.idPost, (post) => {
      this.mostrarAlertaExitosa();
    }, (err) => {
      console.log('Error :: ' , err);
      this.mostrarAlertaFallida();
    });

  }

  // CERRAR LA MODAL DE CONFIRMACION
  irAtras () {
    this.viewCtrl.dismiss();
  }

  // ALERTA PARA INDICAR ELIMINACION EXITOSA DE UN POST
  mostrarAlertaExitosa() {
    const alert = this.alertCtrl.create({
      title: 'Exito',
      subTitle: 'Se elimino el post de manera exitosa!!!',
      buttons: ['Aceptar']
    });
    alert.present();
    this.viewCtrl.dismiss();
  }

  // ALERTA PARA INDICAR ELIMINACION FALLIDA DE UN POST
  mostrarAlertaFallida() {
    const alert = this.alertCtrl.create({
      title: 'Fallida !!!',
      subTitle: 'No se pudo agregar el post !!! Intente Nuevamente ',
      buttons: ['Aceptar']
    });
    alert.present();
  }

}

